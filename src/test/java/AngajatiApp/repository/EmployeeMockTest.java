package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {
    Employee Vasile = new Employee("Dorel", "Georgescu", "1234567890870", DidacticFunction.TEACHER, 2500d);

    @Test
    void modifyEmployeeFunctionTest1() {
        EmployeeMock employeeMock = new EmployeeMock();
        Employee employeeNull = null;
        employeeMock.modifyEmployeeFunction(employeeNull,DidacticFunction.CONFERENTIAR);
        boolean gasit = false;
        int i= 0;
        while(i<employeeMock.getEmployeeList().size()) {
            Employee em = employeeMock.getEmployeeList().get(i);
            if (em.getFunction() == DidacticFunction.CONFERENTIAR)
                gasit = true;
            i++;
            assertEquals(false, gasit);
        }
    }

    @Test
    void modifyEmployeeFunctionTest2() {
        EmployeeMock repoEmployee = new EmployeeMock();
        repoEmployee.modifyEmployeeFunction(Vasile, DidacticFunction.TEACHER);
        assertEquals(DidacticFunction.TEACHER,Vasile.getFunction());
    }
}