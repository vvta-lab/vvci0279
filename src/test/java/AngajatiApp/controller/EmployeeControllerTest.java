package AngajatiApp.controller;

import AngajatiApp.model.Employee;
import AngajatiApp.repository.EmployeeMock;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static AngajatiApp.controller.DidacticFunction.TEACHER;
import static org.junit.jupiter.api.Assertions.*;

class EmployeeControllerTest {

    private Employee e1;
    EmployeeController employeeController;
    int nrEmployee;

    @BeforeEach
    void setUp() {
        e1=new Employee();
        e1.setFirstName("Daniel");
        e1.setLastName("Dan");
        try{
            e1.setSalary(1d);
        } catch(Exception e) {}
        e1.setFunction(TEACHER);
        e1.setCnp("2891212343202");
        EmployeeMock employeeMock = new EmployeeMock();
        EmployeeController employeeController = new EmployeeController(employeeMock);
        try {
            nrEmployee = employeeController.getEmployeesList().size();
        } catch (Exception e) {}
        System.out.println("setUp");
    }

    @AfterEach
    void tearDown() {
        System.out.println("tearDown");
    }

    @Test
    void addEmployeeTC1() {
        try{
            employeeController.addEmployee(e1);
            assert(false);
            assertEquals(nrEmployee+1,employeeController.getEmployeesList().size());
        } catch (Exception e) {
            assert(true);
        }
        System.out.println("addEmployeeTC1");
    }

    @Test
    void addEmployeeTC2() {
        try {
            e1.setSalary(2500d);
            e1.setLastName("Da");
            employeeController.addEmployee(e1);
            assert (false);
        }catch (Exception e){
            assert (true);
            try{
                assertEquals(nrEmployee,employeeController.getEmployeesList().size());
            } catch(Exception e2){}
        }
        System.out.println("addEmployeeTC2");
    }

    @Test
    void addEmployeeTC3() {
        try {
            e1.setSalary(2500d);
            e1.setLastName(null);
            employeeController.addEmployee(e1);
            assert (false);
        }catch (Exception e){
            assert (true);
            try{
                assertEquals(nrEmployee,employeeController.getEmployeesList().size());
            } catch(Exception e2){}
        }
        System.out.println("addEmployeeTC3");
    }

    @Test
    void addEmployeeTC4() {
        try{
            e1.setSalary(-2500d);
            employeeController.addEmployee(e1);
            assert (false);
        }catch (Exception e){
            assert (true);
            try{
                assertEquals(nrEmployee,employeeController.getEmployeesList().size());
            } catch(Exception e2){}
        }
        System.out.println("addEmployeeTC4");
    }

    @Test
    void addEmployeeTC5() {
        try{
            e1.setFirstName("Dana");
            e1.setLastName("Popa");
            e1.setCnp("2891212343202");
            e1.setFunction(DidacticFunction.ASISTENT);
            e1.setSalary(3000d);
            employeeController.addEmployee(e1);
            assert (false);
            assertEquals(nrEmployee+1,employeeController.getEmployeesList().size());
        }catch (Exception e){
            assert (true);
        }
        System.out.println("addEmployeeTC5");
    }

    @Test
    void addEmployeeTC6() {
        try {
            e1.setFirstName("Daniel");
            e1.setLastName("Dan");
            e1.setCnp("2891212343202");
            e1.setFunction(DidacticFunction.TEACHER);
            e1.setSalary(-1d);
            employeeController.addEmployee(e1);
            assert (false);
        }catch (Exception e){
            assert (true);
            try{
                assertEquals(nrEmployee,employeeController.getEmployeesList().size());
            } catch(Exception e2){}
        }
        System.out.println("addEmployeeTC6");
    }
    @Test
    void addEmployeeTC7() {
        try {
            e1.setFirstName("Daniel");
            e1.setLastName("Dan");
            e1.setCnp("2891212343202");
            e1.setFunction(DidacticFunction.TEACHER);
            e1.setSalary(0d);
            employeeController.addEmployee(e1);
            assert (false);
        }catch (Exception e){
            assert (true);
            try{
                assertEquals(nrEmployee,employeeController.getEmployeesList().size());
            } catch(Exception e2){}
        }
        System.out.println("addEmployeeTC7");
    }

    @Test
    void addEmployeeTC8() {
        try {
            e1.setFirstName("Daniel");
            e1.setLastName("Dan");
            e1.setCnp("2891212343202");
            e1.setFunction(DidacticFunction.TEACHER);
            e1.setSalary(2500d);
            employeeController.addEmployee(e1);
            assert (false);
            assertEquals(nrEmployee+1,employeeController.getEmployeesList().size());
        }catch (Exception e){
            assert (true);
        }
        System.out.println("addEmployeeTC8");
    }

}